package edu.uchicago.fenghuayang.protwitterapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import io.fabric.sdk.android.Fabric;

public class ComposeTweetActivity extends AppCompatActivity {

    private static final String TWITTER_KEY = "vXzMQzNZj6wSAvM0EJkv06IXe";
    private static final String TWITTER_SECRET = "6jeFcCtAsGXf2XkzBirUipibvPF8qf6pPVWhi9LQERHO1LtlDV";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_tweet);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());

        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("just setting up my Twitter Kit.");
        builder.show();
    }
}
